import React from 'react';
import Dashboard from './Dashboard';
import TokenInput from './TokenInput';

class App extends React.Component {
    constructor(props) {
        super(props);
        this.graphIRI = null;
        this.token = null;
        this.state = {
            signedIn: false,
        };
    }

    onSubmit(graphIRI, token) {
        this.graphIRI = graphIRI;
        this.token = token;
        this.setState({
            signedIn: true,
        });
    }

    render() {
        if (!this.state.signedIn) {
            return <TokenInput submit={(graphIRI, token) => this.onSubmit(graphIRI, token)} />
        } else {
            return <Dashboard
                graphIRI={this.graphIRI}
                token={this.token}
                onSignOut={() => this.setState({ signedIn: false })}
            />;
        }
    }
}

export default App