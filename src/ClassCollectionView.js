import React from 'react';
import Card from 'react-bootstrap/Card';
import CardColumns from 'react-bootstrap/CardColumns';
import Button from 'react-bootstrap/Button';

export default function ClassCollectionView(props) {
    let cards = props.classItems.map((classContentItem, index) => (
        <Card key={index}>
            <Card.Header>{classContentItem.namespace}</Card.Header>
            <Card.Body>
                <Card.Title>{classContentItem.title}</Card.Title>
                <Card.Text>Full IRI: {classContentItem.resourceIRI}</Card.Text>
                <Button onClick={() => props.onClassClick(classContentItem)}>Details</Button>
            </Card.Body>
        </Card>
    ));

    return (
        <CardColumns>
            {cards}
        </CardColumns>
    );
}