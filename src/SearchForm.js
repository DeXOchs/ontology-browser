import { React, useEffect } from 'react';
import Form from 'react-bootstrap/Form';

export default function SearchForm(props) {
    let contentItemsBackup;
    useEffect(() => {
        contentItemsBackup = props.contentItems;
    }, []);

    function filterItems(searchString) {
        let filteredContentItems = contentItemsBackup
            .slice()
            .filter(contentItem => (
                [searchString].concat(searchString.split(' '))
                    .reduce((acc, pattern) => acc || contentItem.resourceIRI.toLowerCase().includes(pattern.toLowerCase()), false)
            )
        );
        props.onFilterApplied(filteredContentItems);
    }

    return (
        <Form inline>
            <Form.Control
                placeholder="filter content"
                onChange={(e) => filterItems(e.target.value)}
            />
        </Form>
    );
}