import React from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Container from 'react-bootstrap/Container';
import Spinner from 'react-bootstrap/Spinner';
import MockService from './services/MockService.js';
import ClassDetailView from './ClassDetailView';
import ClassCollectionView from './ClassCollectionView';
import Button from 'react-bootstrap/Button';
import Path from './models/Path';
import PathView from './PathView';
import SearchForm from './SearchForm';
import GraphView from './GraphView';

class Dashboard extends React.Component {
    constructor(props) {
        super(props);
        this.graphIRI = this.props.graphIRI;
        this.service = new MockService(this.props.token);
        this.path = new Path();
        
        this.state = {
            classItems: null,
            detailClass: null,
            asGraph: false,
        };
    }

    componentDidMount () {
        this.getClasses();
    }

    getClasses() {
        this.setState({
            classItems: null,
            detailClass: null,
            asGraph: false,
        });
        this.path.clearPath();
        this.service.getClasses(this.graphIRI)
            .then(classItems => {
                this.setState({
                    classItems: classItems,
                })
            });
    }
    
    getTriplesOfClass(classContentItem) {
        this.setState({
            classItems: null,
            detailClass: null,
            asGraph: false,
        });
        this.path.pushNode(classContentItem);
        this.service.getTriplesOfClass(this.graphIRI, classContentItem)
            .then(classItem => {
                this.setState({
                    detailClass: classItem,
                });
            });
    }

    showGraph() {
        this.setState({
            detailClass: null,
            asGraph: true,
        });
    }
    
    render() {
        let searchForm = this.state.classItems && !this.state.asGraph ?
            <SearchForm
                contentItems={this.state.classItems}
                onFilterApplied={filteredClassItems => this.setState({ classItems: filteredClassItems })}
            /> :
            null;

        let graphButton = this.state.classItems ?
            <Button 
                variant="light"
                className="ml-2"
                onClick={() => this.setState({ asGraph: true })}
            >
                Graph
            </Button> :
            null;

        let loadingIndicator = this.state.detailClass || this.state.classItems ?
            null :
            <Spinner className="ml-2" animation="border" variant="primary"/>;
        
        let contentView = null;
        if (this.state.detailClass) {
            contentView = (
                <ClassDetailView
                    detailClass={this.state.detailClass}
                    onClassClick={(classContentItem) => this.getTriplesOfClass(classContentItem)}
                />
            );
        }
        else if (this.state.classItems) {
            if (this.state.asGraph) {
                contentView = (
                    <GraphView
                        getTriples={(classContentItem) => this.service.getTriplesOfClass(this.graphIRI, classContentItem)}
                        classItems={this.state.classItems}
                    />
                );
            } else {
                contentView = (
                    <ClassCollectionView
                        classItems={this.state.classItems}
                        onClassClick={(classContentItem) => this.getTriplesOfClass(classContentItem)}
                    />
                );
            }
        }

        return (
            <>
                <Navbar bg="dark" variant="dark" sticky="top">
                    <Navbar.Brand className="mr-5">Ontology Dashboard</Navbar.Brand>
                    <PathView
                        path={this.path.getMinimalPath()}
                        onPathEntryClicked={(classContentItem) => this.getTriplesOfClass(classContentItem)}
                        onRootClicked={() => this.getClasses()}
                    />
                    <div className="mr-2" />
                    {searchForm}
                    {graphButton}
                    {loadingIndicator}
                    <Button
                        variant="outline-danger"
                        className="ml-auto"
                        onClick={() => this.props.onSignOut()}
                    >
                        Sign Out
                    </Button>
                </Navbar>

                <Container className="w-75 px-0 mt-5 mx-auto mt-0" fluid>
                    {contentView}
                </Container>
            </>
        );
    }
}

export default Dashboard