export default class PropertyNode {
    constructor(id, label) {
        this.id = id;
        this.label = label;
        this.class = 'propertyNode';
    }
}