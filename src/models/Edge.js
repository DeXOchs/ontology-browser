export default class Edge {
    constructor(sourceId, targetId, label) {
        this.source = sourceId;
        this.target = targetId;
        this.label = label;
    }
}