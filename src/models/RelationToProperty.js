import Resource from "./Resource";

export default class RelationToProperty extends Resource {
    constructor(resourceIRI) {
        super(resourceIRI);
        this.propertyTitle = resourceIRI.substring(resourceIRI.lastIndexOf('/') + 1);
    }
}