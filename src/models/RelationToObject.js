import Resource from "./Resource";

export default class RelationToObject extends Resource {
    constructor(resourceIRI, object) {
        super(resourceIRI);
        this.object = object;
    }
}