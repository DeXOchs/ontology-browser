export default class Resource {
    constructor(resourceIRI) {
        this.resourceIRI = resourceIRI;

        let lastIndexSlash = resourceIRI.lastIndexOf('/');
        let lastIndexHash = resourceIRI.lastIndexOf('#');
        let subNamespace = (separatorIndex) => resourceIRI.substring(0, separatorIndex);
        let subTitle = (separatorIndex) => resourceIRI.substring(separatorIndex + 1);

        if (lastIndexHash > lastIndexSlash) {
            this.namespace = subNamespace(lastIndexHash) + '#';
            this.title = subTitle(lastIndexHash);
        } else {
            this.namespace = subNamespace(lastIndexSlash) + '/';
            this.title = subTitle(lastIndexSlash);
        }
    }
}