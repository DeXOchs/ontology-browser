import Resource from "./Resource";

export default class ClassItem extends Resource {
    constructor(resourceIRI) {
        super(resourceIRI);
        this.relations = new Array(0);
    }

    addRelation(relationTo) {
        if (relationTo) this.relations.push(relationTo);
    }
}