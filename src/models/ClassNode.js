export default class ClassNode {
    constructor(id, label, classItem) {
        this.id = id;
        this.label = label;
        this.classItem = classItem;
        this.class = 'classNode';
    }
}