export default class Path {
    constructor() {
        this.init();
    }

    pushNode(contentItem) {
        if (contentItem) {
            this.fullPath.push(contentItem);

            let occurenceIndex = this.minimalPath.map(entry => entry.title).lastIndexOf(contentItem.title);
            if (occurenceIndex >= 0) this.minimalPath = this.minimalPath.splice(0, occurenceIndex);
            this.minimalPath.push(contentItem);
        }
    }

    clearPath() {
        this.init();
    }

    getMinimalPath() {
        return this.minimalPath;
    }

    getMinimalPathLengh() {
        return this.minimalPath.length;
    }

    init() {
        this.fullPath = new Array(0);
        this.minimalPath = new Array(0);
    }
}