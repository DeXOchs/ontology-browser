import DagreGraph from 'dagre-d3-react';
import React from 'react';
import ClassNode from './models/ClassNode';
import Edge from './models/Edge';
import PropertyNode from './models/PropertyNode';
import RelationToObject from './models/RelationToObject';
import RelationToProperty from './models/RelationToProperty';

export default class GraphView extends React.Component {
    constructor(props) {
        super(props);
        this.unseenClassItems = this.props.classItems.slice();
        this.state = {
            classNodes: [],
            propertyNodes: [],
            edges: [],
        };
    }

    componentDidMount() {
        if (this.unseenClassItems.length > 0) {
            let classNode = this.addClassNode(this.unseenClassItems[0]);
            this.traverseTriples(classNode);
        }
    }

    checkUnseenClassItems() {
        this.unseenClassItems = this.unseenClassItems.filter(classItem => 
            !this.state.classNodes.map(classNode => classNode.classItem.resourceIRI).includes(classItem.resourceIRI)
        );
        if (this.unseenClassItems.length > 0) {
            console.log('propose ClassItem ', this.unseenClassItems[0]);
            let classNode = this.addClassNode(this.unseenClassItems[0]);
            this.traverseTriples(classNode);
        }
    }

    addClassNode(classItem) {
        let classNode = new ClassNode(this.computeClassNodeId(classItem), classItem.title, classItem);
        console.log('adding ClassNode: ', classNode);
        this.setState({
            classNodes: this.state.classNodes.concat([classNode]),
        })
        return classNode;
    }

    addPropertyNode(relation) {
        let propertyNode = new PropertyNode(relation.resourceIRI + Date.now(), relation.title);
        console.log('adding PropertyNode: ', propertyNode);
        this.setState({
            propertyNodes: this.state.propertyNodes.concat([propertyNode]),
        });
        return propertyNode;
    }

    addEdge(sourceId, targetId, label) {
        let edge = new Edge(sourceId, targetId, label);
        console.log('adding Edge: ', edge);
        this.setState({
            edges: this.state.edges.concat([edge]),
        });
    }

    traverseTriples(classNode) {
        let traverseObjectTriple = (relation) => {
            let knownClassNode = this.state.classNodes.find(classNode => classNode.id === this.computeClassNodeId(relation.object));
            if (knownClassNode !== undefined) {
                // add new Edge to known ClassNode
                this.addEdge(classNode.id, knownClassNode.id, relation.title);
            } else {
                // add new ClassNode, Edge and traverse this node
                let newClassNode = this.addClassNode(relation.object);
                this.addEdge(classNode.id, newClassNode.id, relation.title);
                this.traverseTriples(newClassNode);
            }
        }
        let traversePropertyTriple = (relation) => {
            let propertyNode = this.addPropertyNode(relation);
            this.addEdge(classNode.id, propertyNode.id, relation.namespace);
        }

        console.log('traversing from ClassNode: ', classNode);
        this.props.getTriples(classNode.classItem).then(classItem => classItem.relations.forEach(relation => {
            if (relation instanceof RelationToObject) traverseObjectTriple(relation);
            else if (relation instanceof RelationToProperty) traversePropertyTriple(relation);
            // after recursion returns, check for unseen classes
            this.checkUnseenClassItems();
        }));
    }

    computeClassNodeId(classItem) {
        return classItem.resourceIRI;
    }

    render() {
        return (
            <>
                <DagreGraph
                    className="m-0 p-0 border"
                    nodes={this.state.classNodes.concat(this.state.propertyNodes)}
                    links={this.state.edges}
                    config={{
                        rankdir: 'LR',
                        ranker: 'network-simplex'
                    }}
                    width='100%'
                    height='80vh'
                    animate={500}
                    shape='circle'
                    fitBoundaries
                    zoomable
                    onNodeClick={e => console.log(e)}
                    onRelationshipClick={e => console.log(e)}
                />
            </>
        );
    }
}