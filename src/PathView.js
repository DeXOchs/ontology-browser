import React from 'react';
import Button from 'react-bootstrap/Button';
import ButtonGroup from 'react-bootstrap/ButtonGroup';

export default function PatView(props) {
    let buttons = props.path.map((entry, index) => (
        <Button
            variant="light"
            key={index} 
            onClick={() => props.onPathEntryClicked(entry)}
        >
            {'> ' + entry.title}
        </Button>
    ));

    return (
        <ButtonGroup>  
            <Button variant="light" onClick={() => props.onRootClicked()}>
                Start
            </Button>
            {buttons}
        </ButtonGroup>
    );
}