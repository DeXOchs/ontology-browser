import React from 'react';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';
import Modal from 'react-bootstrap/Modal';

export default function TokenInput(props) {
    let graphIRI = "";
    let token = "";

    return (
        <Modal
            show={true}
            backdrop="static"
            keyboard={false}
            size="lg"
            centered
        >   
            <Modal.Header>
                <Modal.Title>Setup</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <InputGroup className="mb-3">
                    <Form.Control
                        type="url"
                        onChange={(e) => graphIRI = e.target.value.trim()}
                        placeholder="Graph IRI"
                    />
                </InputGroup>
                <InputGroup>
                    <Form.Control 
                        as="textarea"
                        onChange={(e) => token = e.target.value.trim()}
                        placeholder="Bearer eyZd..."
                    />
                </InputGroup>
            </Modal.Body>
            <Modal.Footer>
            <Button variant="success" onClick={() => props.submit(graphIRI, token)}>
                Submit
            </Button>
            </Modal.Footer>
        </Modal>
    );
}