import React from 'react';
import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import ListGroup from 'react-bootstrap/ListGroup';
import RelationToProperty from './models/RelationToProperty';
import RelationToObject from './models/RelationToObject';

export default function ClassDetailView(props) {
    console.log(props.detailClass);
    let relationsToProperties = props.detailClass.relations
        .filter(relation => relation instanceof RelationToProperty)
        .map((relation, index) => (
            <ListGroup.Item key={index}>
                {relation.namespace}<b>{relation.title}</b>
            </ListGroup.Item>
        ));
    
    let relationsToObjects = props.detailClass.relations
        .filter(relation => relation instanceof RelationToObject)
        .map((relation, index) => (
            <ListGroup.Item key={index}>
                {relation.namespace}<b>{relation.title}</b>
                <Button 
                    onClick={() => props.onClassClick(relation.object)}
                    variant="outline-primary"
                    size="sm"
                    className="ml-3"
                >
                    {relation.object.title}
                </Button>
            </ListGroup.Item>
        ));

    return (
        <Col>
            <Row className="mb-5 d-flex justify-content-center border">
                <ListGroup variant="flush" className="w-100 d-flex align-items-center">
                    <ListGroup.Item><h3>{props.detailClass.title}</h3></ListGroup.Item>
                    <ListGroup.Item>{props.detailClass.resourceIRI}</ListGroup.Item>
                </ListGroup>
            </Row>

            <Row>
                <Col xs={4} className="p-4 mr-5 border rounded">
                    <ListGroup variant="flush">
                        <ListGroup.Item><h5>Properties</h5></ListGroup.Item>
                        {relationsToProperties}
                    </ListGroup>
                </Col>
                <Col className="p-4 border rounded">
                    <ListGroup variant="flush">
                        <ListGroup.Item><h5>To Objects</h5></ListGroup.Item>
                        {relationsToObjects}
                    </ListGroup>
                </Col>
            </Row>
        </Col>
    );
}