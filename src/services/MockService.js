import ClassItem from '../models/ClassItem.js';
import RelationToObject from '../models/RelationToObject.js';
import RelationToProperty from '../models/RelationToProperty.js';

export default class MockService {
    constructor(token) {
        this.token = token;
    }

    async getClasses(graphIRI) {
        return [
            new ClassItem("namespace:IRI")
        ];
    }
    
    async getTriplesOfClass(graphIRI, classContentItem) {
        let newClassContentItem = new ClassItem(classContentItem.resourceIRI);
        newClassContentItem.addRelation(new RelationToProperty("namespace:IRI"));
        newClassContentItem.addRelation(new RelationToObject("namespace:IRI", new ClassItem("namespace:IRI")));

        return newClassContentItem;
    }
}